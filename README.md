#This project is aimed to analyse the Sydney restaurant data from Zomato website. 
#The project is gonna produce 5 models to predict the success of a restaurant in Sydney.
#The models are built by machine learning algorithms such as Linear Regression, Logistic Regression, SVM and Random Forest.
#Please open the file to see the result.
